package com.lizhuhui.myapplication


/**
 * @author  LiZhuHui
 * @time    2018/12/17 9:50
 * @des        ${111}
 */
class MyTest {
    object MyTest {
        fun i(string: String) {
            println(string)
        }

        fun testMutableList(list: MutableList<String>) {
            list.add("香港")
            if (list.contains("上海")) {
                list.remove("上海")
            }
            println(list)
            println(list.drop(3))
            val index = list.iterator()
            while (index.hasNext()) {
                println(index.next())
            }
            println("for-each---------------------------------------")
            list.forEach {
                println(it)
            }
        }
    }


}