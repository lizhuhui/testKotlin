package com.lizhuhui.myapplication

import android.text.Editable
import android.text.TextWatcher

/**
 * @author LiZhuHui
 * @time 2018/12/17 10:36
 * @des ${TODO}
 */

class Test {
    internal var TAG = this.javaClass.simpleName
    internal var mWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {

        }
    }
}
