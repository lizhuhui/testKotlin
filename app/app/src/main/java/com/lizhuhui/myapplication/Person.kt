package com.lizhuhui.myapplication


/**
 * @author  LiZhuHui
 * @time    2018/12/17 10:02
 * @des        ${我么}
 */
data class Person(val userName: String, val age: Int) {
    private val userName1: String = userName
    private val age1: Int = age
}