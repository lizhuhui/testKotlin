package com.lizhuhui.myapplication

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout


/**
 * @author  LiZhuHui
 * @time    2018/12/17 9:57
 * @des        ${自定义布局 }
 */
class MyLayout:RelativeLayout{
    @JvmOverloads
    constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0):super(context, attributeSet, defStyleAttr)
}