package com.lizhuhui.myapplication

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val TAG:String=this.javaClass.simpleName

    var adapter: MyAdapter? = null
    var list = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val string1: String = "string1"
        val string2: String? = "null"
        val string3: String? = "string3"
        println(string1?.length)
        println(string2?.length)
        println(string3?.length)
        string2?.let {
            println(string2.length)
            list.add("one")
            list.add("two")
            list.add("three")
            adapter = MyAdapter(this@MainActivity, list)
            myList.adapter = adapter

        }
        if (tv_show is TextView) {
            tv_show.setText(" hello!!f 5 4 ")
            var text = tv_show.text;
            text = text.trim { it <= ' ' }
            Log.i("TAG", "text=" + text.length + text)
        }
        testWhen(1)
        testWhen("sss")
        MyTest.MyTest.i("我是辉哥")
        var myLayout=MyLayout(this)
        var person=Person("lizhuhui",11)
        Log.i(TAG,"person.age="+person.age)
        val nollNewsStations = mutableListOf("北京","上海","广东","杭州","天津","四川","重庆","湖北","广西","河南","河北")
        MyTest.MyTest.testMutableList(nollNewsStations)
        Log.i(TAG,"nollNewsStations.simpleName="+nollNewsStations.javaClass.simpleName)

    }

    fun testWhen(obj: Any) {
        when (obj) {
            is Int -> {
                println("obj is a int")
            }
            is String -> {
                println("obj is a string")
            }
            else -> {
                println("obj is other type")
            }

        }
    }

    fun testWhen(int: Int) {
        when (int) {

            in 10..Int.MAX_VALUE -> println("${int} 太大了我懒得算")
            2, 3, 5, 7 -> println("${int} 是质数")
            else -> println("${int} 不是质数")
        }
    }


    inner class MyAdapter(context: Context, list: List<String>) : BaseAdapter() {
        private val context = context
        private val list = list


        override fun getItem(position: Int): Any {
            return list.get(position)

        }

        override fun getItemId(position: Int): Long {
            return position.toLong()

        }

        override fun getCount(): Int {
            return list.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView;
            view = LayoutInflater.from(context).inflate(R.layout.item_list_my, null)
            var textView: TextView = view.findViewById(R.id.tv_text)
            textView.text = list.get(position)
            return view
        }
    }

}
